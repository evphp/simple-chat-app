window.onload = function() {
  
  var message = document.getElementById("_message");
  var btnSend = document.getElementById("_send");

  var socket = io();

  btnSend.onclick = function () {
    socket.emit('message', message.value);
  }

  socket.on('message', function(msg){
    $('#_messages').append($('<li>').text(msg));
  });

}