'user strict';

/* Controllers */
var chatApp = angular.module('chatApp', []);

chatApp.controller('ChatCtrl', ['$scope', function ($scope) {

	$scope.server = 'http://127.0.0.1:8000';
	$scope.socket = undefined;
	$scope.user_id = '';
	$scope.user_name = '';
	$scope.is_logged = false;
	$scope.user_status = 'undef';
	$scope.user_name_valid = undefined;

	$scope.input = {
		'chat': $('#inputChat'),
		'text': $('#inputText')
	};

	$scope.users = [];

	var usersHash = {};

	var check = function() {
		if ($scope.user_name.length > 2 && $scope.user_name.length < 11) {
			$scope.user_name_valid = true;
		} else {
			$scope.user_name_valid = false;
		}
		return $scope.user_name_valid;
   };

	$scope.logInOff = function() {
		if ($scope.is_logged) logoff();
		else  login();
	};

	$scope.setStatus = function(status) {
		if ($scope.is_logged) {
			$scope.socket.emit('cmd', {'cmd': 'change_status','status': status}, function(resp) {
				if (resp == 'success') {
					$scope.user_status = status;
					$scope.$apply();
				}
    		});
		}
	};

	$scope.send = function() {
		if ($scope.is_logged) {
			$scope.socket.emit('message', $scope.input.text.val(), function(resp) {
				if (resp == "success") {
					addMsg($scope.input.text.val(), $scope.user_name, $scope.user_status);
					$scope.input.text.val('');
				}
     		});
		}
	};

	$scope.getName = function(name) {
		var text = $scope.input.text.val();
		$scope.input.text.val(text + (text ? ' ' : '') + name + ' ');
	};

	var login = function() {
		if (check()) {
			$scope.socket = io.connect($scope.server);
    		$scope.socket.emit('login', {'name': $scope.user_name, 'status': $scope.user_status}, function(resp) {
    			if (resp.result = 'success') {
    				$scope.user_id = resp.id;
    				$scope.is_logged = true;
    				$scope.socket.emit('cmd', {'cmd': 'get_peers'}, function(peers) {
    					$scope.users = peers;
    					$scope.$apply(); 
    					listner();
		    		});

    			} else {
    				console.log('USER LOGIN ERROR: ' + resp);
    			}
    		});
		}
	};

	var listner = function() {
		$scope.socket.on('peers_update', function(peers) {
		    $scope.users = getPeers(peers, $scope.user_id);
    		$scope.$apply(); 
		});
		$scope.socket.on('message', function(resp) {
			if (resp.id) {
		    	var user = getPeerData(resp.id);
		    	addMsg(resp.message, user.name, user.status);
    		}
		});
		$scope.socket.on('update_status', function(resp) {
		    updateStatus(resp.status, resp.id);
    		$scope.$apply(); 
		});
		$scope.socket.on('user_disconnected', function(resp) {
			var peer = getPeerData(resp.id);
			var name = peer.name;
			$scope.users = getPeers($scope.users, resp.id);
    		$scope.$apply(); 
    		addDisconnectMsg(name);
		});
	};

	var addMsg = function(msg, user_name, user_status) {
		var userHtml = '<span class=\'status' + user_status.substr(0,1).toUpperCase() + user_status.substr(1) + '\'>' + user_name + '</span>: ';
		$scope.input.chat.prepend('<p>' + userHtml + msg + '</p>');
	};

	var addDisconnectMsg = function(name) {
		$scope.input.chat.prepend('<p class=\'warn\'>User '  + name + ' disconnected...</p>');
	};

	var logoff = function(socket) {
		$scope.socket.disconnect();
		$scope.is_logged = false;
		$scope.users = [];
		$scope.user_name = '';
		$scope.is_logged = false;
		$scope.user_status = 'undef';
		$scope.user_name_valid = undefined;
		$scope.input.chat.html('');
		$scope.input.text.val('');
	};

	var getPeers = function(peers, id) {
		var peersRes = Array();
		for (key in peers) {
			if (peers[key].id == id) continue;
			peersRes.push(peers[key]);
		}

		return peersRes;
	};

	var getPeerData = function(id) {
		var i = 0;
		for (key in $scope.users) {
			if ($scope.users[key].id == id) {
				i = key;
				break;
			}
		}
		return $scope.users[i];
	};

	var updateStatus = function(status, id) {
		for (key in $scope.users) {
			if ($scope.users[key].id == id) {
				$scope.users[key].status = status;
				break;
			}
		}
	};

}]);
