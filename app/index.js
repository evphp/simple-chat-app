var express = require('express'); 
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use('/static', express.static(__dirname + '/js'));
app.use('/static', express.static(__dirname + '/css'));
app.use('/fonts', express.static(__dirname + '/fonts'));
app.use('/static', express.static(__dirname + '/bower_components'));

app.get('/', function(req, res) {
  res.sendFile('index.html' , { root : __dirname});
});


var peers = [];

io.on('connection', function(socket) {

	//console.log('a user connected');
  	socket.on('disconnect', function() {
  		var id = getId(socket.id);
  		//console.log('user('+id+') disconnected');
  		peers = getPeers(peers, id); // update peers array
  		socket.broadcast.emit('user_disconnected', {'id': id});
  	});

	// new user connected
	socket.on('login', function(userObj, callback) {
    	var id = getId(socket.id);
    	peers.push({'id': id, 'name': userObj.name, 'status': userObj.status});
    	callback({'result': 'success', 'id': id});
    	socket.broadcast.emit('peers_update', peers);
	});

	// cmd - get_peers,....
	socket.on('cmd', function(data, callback) {
  		var id = getId(socket.id);
  		if (data.cmd == 'get_peers') {
  			callback(getPeers(peers, id));
  		}
  		if (data.cmd == 'change_status') {
  			updateStatus(data.status, id, peers);
  			socket.broadcast.emit('update_status', {'id': id, 'status': data.status});
  			callback('success');
  		}
  	});

  	// message
  	socket.on('message', function(msg, callback) {
  		var id = getId(socket.id);
		socket.broadcast.emit('message', {'id': id,'message': msg});
  		callback("success");
  	});

});

http.listen(8000, function() {
	console.log('listening on *:8000');
});


// service functions
var getId = function(socketId) {
	return (socketId).toString().substr(0, 5);
};

var getPeers = function(peers, id) {
	var peersRes = Array();
	for (key in peers) {
		if (peers[key].id == id) continue;
		peersRes.push(peers[key]);
	}

	return peersRes;
};

var updateStatus = function(status, id, peers) {
	for (key in peers) {
		if (peers[key].id == id) {
			peers[key].status = status;
			break;
		}
	}
};


